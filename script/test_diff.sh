#!/usr/bin/env bash
# ------------------------------------------------------------------------------
#
# Tests for ../util/diff_to_terminal.sh
#
# ------------------------------------------------------------------------------

# diff files
../util/diff_to_terminal.sh -f test_diff_file1.txt test_diff_file2.txt

# diff strings
../util/diff_to_terminal.sh -s "tuvwxyz" "tu3wx6z"
../util/diff_to_terminal.sh -s "tuvwxyz" "tu3wx 6z"
../util/diff_to_terminal.sh -s tuvwxyz tu3wx6z

# invalid flag
../util/diff_to_terminal.sh -x "invalid" "flag"

# ------------------------------------------------------------------------------
