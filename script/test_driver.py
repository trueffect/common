#!/usr/local/bin/python3
# ------------------------------------------------------------------------------
#
#   Test Driver
#
# ------------------------------------------------------------------------------

import sys
import os

# add 'common/lib' dir to system path
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(MY_REPO_DIR + os.sep + 'lib')

# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
AT_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

from google_sheets_api import read_worksheet, read_worksheet_range


# -----------------------------------------------------------
def print_list(lst):
# -----------------------------------------------------------
    if lst:
        row_count = 0
        for row in lst:
            row_count += 1
            print('[ROW ' + str(row_count) + ']: ' + str(row))

# ------------------------------------------------------------------------------


# # -----------------------------------------------------------
# #   Read used range from 'Google Sheets' sheet on Google Drive
# # -----------------------------------------------------------
#
# # id and name of the google spreadsheet
# google_spreadsheet_id = '1NLQMfTA2rIBdjNtp1ZtrS8eR2UQqvwcgoTT1wz6Vlc0'  # AdServer Tests
# worksheet_name = 'sample'   # ad server
#
# # call api, print data
# print_list(read_worksheet(google_spreadsheet_id, worksheet_name))


# -----------------------------------------------------------
#   Read range to specified column from 'Google Sheets' sheet on Google Drive
# -----------------------------------------------------------

# id and name of the google spreadsheet
google_spreadsheet_id = '1NPOyxoWqFmhZ1c-miWlp-2cnQI_T3X9hZP34qnMnai4'  # Pipeline Tests
worksheet_name = 'test-sheet'   # pipeline

# call api, print data
print_list(read_worksheet(google_spreadsheet_id, worksheet_name))
print_list(read_worksheet_range(google_spreadsheet_id, worksheet_name, 1, 1, 0, 10))

# # test exceptions
# print_list(read_worksheet(google_spreadsheet_id + 'blah', worksheet_name + 'blah'))
# print_list(read_worksheet(google_spreadsheet_id, worksheet_name + 'blah'))
#
# # don't print info
# print('========== log_info=False ==========')
# print_list(read_worksheet(google_spreadsheet_id, worksheet_name, False))
# print_list(read_worksheet_range(google_spreadsheet_id, worksheet_name, 1, 1, 0, 10, False))
# print_list(read_worksheet(google_spreadsheet_id + 'blah', worksheet_name + 'blah', False))
# print_list(read_worksheet(google_spreadsheet_id, worksheet_name + 'blah', False))


# ------------------------------------------------------------------------------
