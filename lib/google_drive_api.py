#!/usr/bin/python
# setup:  https://developers.google.com/drive/v3/web/quickstart/python#prerequisites
# api reference:  https://developers.google.com/drive/v3/reference/
# upload ref:  https://google-api-python-client.googlecode.com/hg/docs/epy/apiclient.http.MediaFileUpload-class.html

import os
import httplib2
from apiclient import discovery
from apiclient import http
import oauth2client
from oauth2client import client
from oauth2client import tools
from oauth2client.service_account import ServiceAccountCredentials

try:
    import argparse
    CREDS_FLAGS = argparse.Namespace()
    CREDS_FLAGS.auth_host_name = 'localhost'
    CREDS_FLAGS.auth_host_port = [8080, 8090]
    CREDS_FLAGS.logging_level = 'ERROR'
    CREDS_FLAGS.noauth_local_webserver = False
except ImportError:
    CREDS_FLAGS = None

import google_sheets_api

# credentials
CREDS_DIR = '.credentials'
CREDS_FILE = 'te-automation-drive-access.json'
#CREDS_FILE = 'te-eng-svc-acct-drive.json'

MY_PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/te-automation-drive-access.json
#SCOPES = 'https://www.googleapis.com/auth/drive'
SCOPES = ['https://www.googleapis.com/auth/sqlservice.admin', 'https://www.googleapis.com/auth/drive', 'https://spreadsheets.google.com/feeds']
CLIENT_SECRET_FILE = MY_PROJECT_DIR + os.sep + 'data' + os.sep + CREDS_FILE
APPLICATION_NAME = 'Drive API'

credentials = None

# -----------------------------------------------------------
def get_credentials():
    global credentials
    if not credentials or credentials.invalid:
        print('Getting credentials')
        credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRET_FILE, SCOPES)
    else:
        print('Using existing credentials')
    return credentials

# -----------------------------------------------------------
def read_file_as_list(drive_file_id):
    print('========================================')
    print('Reading file from drive...')
    print('drive_file_id: ' + drive_file_id)

    # get credentials
    print('getting credentials...')
    credentials = get_credentials()
    print('authorizing credentials...')
    http_creds = credentials.authorize(httplib2.Http())
    print('creating service...')
    service = discovery.build('drive', 'v3', http=http_creds)

    # read lines to list
    print('reading data...')
    lines = service.files().get_media(fileId=drive_file_id).execute().decode("utf-8").split('\n')

    # return list of lines
    return lines

# -----------------------------------------------------------
def upload_file(file_path, file_name, drive_folder_id):
    print('========================================')
    print('Uploading file to drive...')
    print('file_path: ' + file_path)
    print('file_name: ' + file_name)
    print('drive_folder_id: ' + drive_folder_id)

    # get credentials
    credentials = get_credentials()
    http_creds = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http_creds)

    # set metadata
    file_metadata = {
        'name': file_name,
        'parents': [drive_folder_id]
    }
    # set data
    media = http.MediaFileUpload(file_path + file_name, resumable=False)

    # upload file
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()

    # return the file id
    print('Upload Complete, File ID: %s' % file.get('id'))
    return file.get('id')

# -----------------------------------------------------------
# TODO: this is a PITA, need to probably create a file and use update_file_from_file()
def update_file_from_list(data_lines, drive_file_id):
    print('========================================')
    print('Updating file on drive...')
    print('len(lines): ' + str(len(data_lines)))
    print('drive_file_id: ' + drive_file_id)

    # get credentials
    credentials = get_credentials()
    http_creds = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http_creds)

    # updated content
    data = "\n".join(data_lines) + "\n"
    print('data:')
    print(data)

    # send the request to the API
    #service.files().update(fileId=drive_file_id, body=data_lines, newRevision=new_revision, media_body=media_body).execute()
    r = service.files().update(fileId=drive_file_id, body=data).execute()
    print('result:')
    print(r)

# -----------------------------------------------------------
def update_file_from_file(source_file, drive_file_id):
    print('========================================')
    print('Updating file on drive...')
    print('drive_file_id: ' + drive_file_id)

    # get credentials
    credentials = get_credentials()
    http_creds = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http_creds)

    # get new media
    media = http.MediaFileUpload(source_file,
                            mimetype='text/plain',
                            resumable=False)

    # send the request to the API
    service.files().update(fileId=drive_file_id, media_body=media).execute()

# -----------------------------------------------------------
