#!/usr/local/bin/python3
#
# --------------------------------------------------------------------------------------------------
#
#   Purpose: This is a wrapper library for accessing TruEffect's CMS API
#
#   CMS API Endpoints:
#
# --------------------------------------------------------------------------------------------------
#

# imports
import subprocess
import shutil

import requests
import json
import datetime
import os
from time import sleep
MY_REPO_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# path to 'automated-tests' project dir (parent of MY_REPO_DIR)
MY_PROJECT_DIR = os.path.dirname(MY_REPO_DIR)

COMMON_LIB_DIR = MY_PROJECT_DIR + os.sep + 'common' + os.sep + 'lib' + os.sep

bar = '================================================================================'

# --------------------------------------
# DEFAULT Settings
# --------------------------------------

# DEFAULT API Endpoint, use set_api_env(protocol, server, version) to override
api_protocol = 'https'
api_server = 'my-stg.trueffect.com'
api_base_uri = api_protocol + '://' + api_server
api_version = 'latest'
# api_version = 'edge'
# api_version = '2.25.2-rc'
#api_version = '2.25.0-rc'
api_uri = api_base_uri + '/cms/' + api_version
oauth_uri = api_base_uri + '/oauth/' + api_version + '/accesstoken'

# DEFAULT API User, use set_user_creds(email, password) to override
# api_user = 'wylie.butler@trueffect.com'
# api_pass = 'Trueffect123'
# api_user = 'gburke1@trueffect.com'
# api_pass = 'gburke'
api_user = 'endtoend@trueffect.com'     # used for creative versioning in STG
api_pass = 'Password123'

# Output Defaults
# print('----------------------------------------')
# print('[DEFAULT] api_uri:   ' + api_uri)
# print('[DEFAULT] oauth_uri: ' + oauth_uri)
# print('[DEFAULT] api_user:  ' + api_user)
# print('[DEFAULT] api_pass:  ' + api_pass)


# -----------------------------------------------------------
# set the api environment (uses default 'https://my-stg.trueffect.com/cms/latest' if not specified
def set_api_env(protocol, server, version):
    print('----------------------------------------')
    print('Setting API Environment Endpoints...')

    global api_protocol
    global api_server
    global api_base_uri
    global api_version
    global api_uri
    global oauth_uri

    # CMS API Endpoint
    api_protocol = protocol
    api_server = server
    api_base_uri = api_protocol + '://' + api_server
    api_version = version
    api_uri = api_base_uri + '/cms/' + api_version
    print('api_uri:   ' + api_uri)

    # OAuth API Endpoint
    oauth_uri = api_base_uri + '/oauth/' + api_version + '/accesstoken'
    print('oauth_uri: ' + oauth_uri)

    return

# -----------------------------------------------------------
# set user credentials for accessing the api (uses default 'endtoend@trueffect.com' if not specified)
def set_user_creds(email, password):
    print('----------------------------------------')
    print('Setting API Credentials...')

    global api_user
    global api_pass

    # User Credentials
    api_user = email
    api_pass = password
    print('api_user: ' + api_user)
    print('api_pass: ' + api_pass)
    print('----------------------------------------')

    return

# -----------------------------------------------------------
# get the user's access token, uses basic auth
def get_access_token():
    def_sig = 'cms_api.get_access_token()'
    print('==>  in ' + def_sig)
    print(' - uri: ' + oauth_uri)
    print(' - api_user: ' + api_user)
    print(' - api_pass: ' + api_pass)

    # call oauth
    response = requests.post(oauth_uri, auth=requests.auth.HTTPBasicAuth(api_user, api_pass))
    print(response)
    # parse the json response
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    # print('response:' + str(json_str))
    # print(j)

    # extract the accessToken and expiresIn values
    accessToken = j['accessToken']
    print('accessToken: ' + accessToken)
    expiresIn = j['expiresIn']
    print('expiresIn: ' + str(expiresIn))

    # return the accessToken
    print('<== out ' + def_sig)
    return accessToken

# -----------------------------------------------------------
# get the api version, no authorization needed
def get_api_version():
    method = '/Status'
    uri = api_uri + method
    print('Calling API [GET]: ' + uri)
    response = requests.get(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    version = j['version']
    print('API Version: ' + version)
    return version


# -----------------------------------------------------------
# calls the API and handles any bad responses or expired tokens
def get_api_response(uri):
    def_sig = 'cms_api.get_api_response(uri)'
    print('==>  in ' + def_sig)
    response = None
    print('Calling API [GET]: ' + uri)
    try:
        # get the user token
        headers = {'Authorization': 'Bearer ' + get_access_token()}
        response = requests.get(uri, headers=headers)
        print('response.status_code: ' + str(response.status_code))
        print('response.text: ' + str(response.text))
        # check for expired token
        if str(response.status_code) == '401':
            print('Token is Expired!  Getting a new token.')
            sleep(2)
            headers = {'Authorization': 'Bearer ' + get_access_token()}
            response = requests.get(uri, headers=headers)
            print('response.status_code: ' + str(response.status_code))
            print('response.text: ' + str(response.text))
    except Exception as e:
        print('ERROR: Unexpected Exception!')
        print(str(e))
        print(str(e.args))

    # return the response
    if not response:
        print('ERROR: Empty response!')
        print('<== out ' + def_sig)
        return None
    else:
        print(response)
        print('<== out ' + def_sig)
        return response

# -----------------------------------------------------------
def get_advertisers():
    method = '/Advertisers'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    return j

# -----------------------------------------------------------
def get_agencies(agency_id):
    method = '/Agencies/' + agency_id
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    return j

# -----------------------------------------------------------
def get_creative(creative_id):
    method = '/Creatives/' + creative_id
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    return j

# -----------------------------------------------------------
def get_creative_version(creative_id):
    method = '/Creatives/' + creative_id
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    creative_version = j['creativeVersion']
    #print('creative_version: ' + str(creative_version))
    return int(creative_version)

# -----------------------------------------------------------
def get_creative_group_creative(creative_group_id):
    method = '/CreativeGroups/' + creative_group_id
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    return j


# -----------------------------------------------------------
def increment_creative_version(creative_id, campaign_id, filename, creative_group_id, alias=None):
    print(bar)
    print(bar)
    def_sig = 'cms_api.increment_creative_version(creative_id, campaign_id, filename, creative_group_id, alias=None)'
    print('==>  in ' + def_sig)
    cv = get_creative_version(creative_id)

    # alias name
    if not alias:
        alias = 'v' + str(cv+1) + '_' + str(datetime.datetime.utcnow().strftime('%Y%m%d%H'))
    print('--------------------')
    print('Setting version to ' + str(cv+1))
    print('Setting alias to ' + alias)

    # upload the new creative
    upload_creative(campaign_id, filename)

    # associate
    associate_creative(filename, alias, campaign_id, creative_group_id)

    print('<== out ' + def_sig)
    return cv+1

# -----------------------------------------------------------
def upload_creative(campaign_id, filename):
    print(bar)
    def_sig = 'cms_api.upload_creative(campaign_id, filename)'
    print('==>  in ' + def_sig)

    # api endpoint
    method = '/Campaigns/' + campaign_id + '/creativeUpload?filename=' + filename
    uri = api_uri + method

    # cURL command
    cmd = "curl -i -X POST -H \"Authorization:Bearer " + get_access_token() + \
          "\" -H \"Content-Type:application/octet-stream\" -T \"" + COMMON_LIB_DIR + filename + "\" '" + uri + "'"
    print('Calling cURL command: ' + cmd)

    # execute cURL command, wait for it to complete
    result = os.popen(cmd).read()
    print(result)

    print(' - done')
    print('<== out ' + def_sig)
    return result

# -----------------------------------------------------------
def associate_creative(filename, alias, campaign_id, creative_group_id):
    print(bar)
    def_sig = 'cms_api.associate_creative(filename, alias, campaign_id, creative_group_id)'
    print('==>  in ' + def_sig)

    method = '/CreativeGroups/createAssociations'
    uri = api_uri + method
    print('Calling API [PUT]: ' + uri)

    # headers = {
    #     'Authorization': 'Bearer ' + get_access_token(),
    #     'Content-Type': 'application/json',
    #     'Accept': 'application/json'
    # }
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'authorization': 'Bearer ' + get_access_token(),
        'Content-Type': 'application/json;charset=UTF-8',
        'Origin': 'https://my-stg.trueffect.com',
        'Referer': 'https://my-stg.trueffect.com/',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'
    }

    body = {
        "campaignId": campaign_id,
        "creatives": [{
                "alias": alias,
                "filename": filename
            }
        ],
        "creativeGroupIds": []
    }

    response = requests.put(uri, json=body, headers=headers)
    print('response.status_code: ' + str(response.status_code))
    # check for expired token
    if str(response.status_code) == '401':
        print('Token is Expired!  Getting a new token.')
        sleep(2)
        # headers = {
        #     'Authorization': 'Bearer ' + get_access_token(),
        #     'Content-Type': 'application/json',
        #     'Accept': 'application/json'
        # }
        headers = {
            'Accept': 'application/json, text/plain, */*',
            'Authorization': 'Bearer ' + get_access_token(),
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://my-stg.trueffect.com',
            'Referer': 'https://my-stg.trueffect.com/',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'
        }
        response = requests.put(uri, json=body, headers=headers)
        print('response.status_code: ' + str(response.status_code))

    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    print('<== out ' + def_sig)
    return j


# -----------------------------------------------------------
def get_agency_metrics_by_campaigns_current_day(agency_id, campaign_id):
    method = '/Agencies/' + agency_id + '/metrics/campaigns'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    j = json.loads(json_str)
    print(j)
    # parse json for each metric record
    metrics = j['records'][0]['Metrics']
    #len_metrics = len(metrics)
    # print('len_metrics: ' + str(len_metrics))
    ctr = None
    id = None
    conversions = None
    day = None
    clicks = None
    cost = None
    impressions = None
    for metric in metrics:
        if int(metric['id']) == int(campaign_id):
            ctr = metric['ctr']
            id = metric['id']
            conversions = metric['conversions']
            day = metric['day']
            clicks = metric['clicks']
            cost = metric['cost']
            impressions = metric['impressions']

    print('******************************')
    print('last record for campaign: ' + str(campaign_id))
    print('******************************')
    print('ctr: ' + str(ctr))
    print('id: ' + str(id))
    print('conversions: ' + str(conversions))
    print('day: ' + str(day))
    print('clicks: ' + str(clicks))
    print('cost: ' + str(cost))
    print('impressions: ' + str(impressions))

    return [impressions, clicks, day]

# -----------------------------------------------------------
def get_agencies_metric_timestamp_cass():
    method = '/Agencies/metric/timestamp/cassandra'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    print(json_str)
    return json_str

# -----------------------------------------------------------
def get_agencies_metric_timestamp_tdm():
    method = '/Agencies/metric/timestamp/tdm'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    print(json_str)
    return json_str


# ------------------------------------------------------------------------------
#   Intraday Metrics
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
def get_campaign_metric_intraday(campaign_id, utc_dy):
    def_sig = 'cms_api.get_campaign_metric_intraday(campaign_id, utc_dy)'
    print('==>  in ' + def_sig)
    method = '/Campaigns/' + campaign_id + '/intraday/metrics'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    #print(json_str)

    j = json.loads(json_str)
    #print(j)

    # parse json for each metric record, add to list of lists
    campaign_lol = []   # list of lists
    metrics = j['records'][0]['Metrics']
    for metric in metrics:
        if int(metric['id']) == int(campaign_id):
            impressions = metric['impressions']
            clicks = metric['clicks']
            hourId = metric['hourId']
            day = metric['day']

            # if the UTC days match, add it to the list of lists
            dy = day[:4] + day[5:7] + day[8:10]
            if utc_dy == dy:
                campaign_lol.append([hourId, impressions, clicks])

    print('Campaign list of lists:')
    campaign_lol = campaign_lol[::-1]   # reverse the list
    print(campaign_lol)
    print('<== out ' + def_sig)
    return campaign_lol

# ------------------------------------------------------------------------------
def get_agency_metric_intraday(agency_id, utc_dy, campaign_list):
    def_sig = 'cms_api.get_agency_metric_intraday(agency_id, utc_dy, campaign_list)'
    print('==>  in ' + def_sig)
    method = '/Agencies/' + agency_id + '/intraday/metrics'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    #print(json_str)

    j = json.loads(json_str)
    #print(j)

    # parse json for each metric record, add to list of lists
    agency_lol = []   # list of lists
    metrics = j['records'][0]['Metrics']
    # print('metrics:')
    # print(metrics)
    for metric in metrics:
        # print('--------------------')
        # print(metric)
        if int(metric['id']) in campaign_list:
            impressions = metric['impressions']
            clicks = metric['clicks']
            hourId = metric['hourId']
            day = metric['day']

            # if the UTC days match, add it to the list of lists
            dy = day[:4] + day[5:7] + day[8:10]
            if utc_dy == dy:
                agency_lol.append([metric['id'], hourId, impressions, clicks])

    print('Agency list of lists:')
    agency_lol = agency_lol[::-1]   # reverse the list
    print(agency_lol)

    print('<== out ' + def_sig)
    return agency_lol

# ------------------------------------------------------------------------------
def get_campaign_metric_intraday_placements(campaign_id, placement_list=None):
    def_sig = 'cms_api.get_campaign_metric_intraday_placements(campaign_id, placement_list=None)'
    print('==>  in ' + def_sig)
    method = '/Campaigns/' + campaign_id + '/intraday/placements/metrics'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    print(json_str)
    j = json.loads(json_str)
    # print(j)

    # parse json for each metric record, add to list of lists
    placement_lol = []   # list of lists
    metrics = j['records']
    for metric in metrics[0]['PlacementIntradayMetrics']:
        # {'currentClicks': 0, 'dailyClicks': 117, 'dailyImpressions': 165, 'id': 5982883, 'currentImpressions': 0, 'name': 'TheSamba - BeetleSection - 259x195'}
        # print('>>>>>>>>>>>>>>>>>>>>')
        # print(metric)
        currentClicks = metric['currentClicks']
        dailyClicks = metric['dailyClicks']
        currentImpressions = metric['currentImpressions']
        dailyImpressions = metric['dailyImpressions']
        id = metric['id']
        #name = metric['name']
        list_to_append = [id, currentImpressions, currentClicks, dailyImpressions, dailyClicks]
        if placement_list:
            if int(id) in placement_list:
                placement_lol.append(list_to_append)
        else:
            placement_lol.append(list_to_append)

    # all done
    lol = sorted(placement_lol)         # sort the list before returning
    print('Placement list of lists:')
    print('| id, currentImpressions, currentClicks, dailyImpressions, dailyClicks |')
    print(lol)
    print('<== out ' + def_sig)
    return lol


# ------------------------------------------------------------------------------
#   Tag Injection
# ------------------------------------------------------------------------------

# -----------------------------------------------------------
def get_htmlInjectionTags(id):
    method = '/HtmlInjectionTags/' + str(id)
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    print(json_str)
    return json_str

# -----------------------------------------------------------
def get_agencies_htmlInjectionTags(agency_id):
    def_sig = 'get_agencies_htmlInjectionTags(agency_id)'
    print('==>  in ' + def_sig)
    method = '/Agencies/' + str(agency_id) + '/htmlInjectionTags'
    uri = api_uri + method
    response = get_api_response(uri)
    json_str = response.text.replace("'", '"')
    print(json_str)
    print('<== out ' + def_sig)
    return json_str


# -----------------------------------------------------------

#upload_creative('5982878', 'rat-beetle_259x195.jpg')


# agency_id = '5975435'
# campaign_id = '5982878'
# get_agency_metric_intraday(agency_id)
# get_campaign_metric_intraday(campaign_id)


# Intraday Placements
#get_campaign_metric_intraday_placements(campaign_id)

# campaign_id = '5982878'
# placement_list = [5982883]
# #placement_list = [5982883, 6455610]
# get_campaign_metric_intraday_placements(campaign_id, placement_list)


# # get_agencies(agency_id)
# # get_agency_metrics_by_campaigns_current_day(agency_id, campaign_id)
# get_agencies_metric_timestamp_cass()
# get_agencies_metric_timestamp_tdm()

# # end to end creative
# campaign_id = '5982878'
# alias = 'testing'
# filename = 'rat-beetle_259x195.jpg'
# creative_id = '5982897'
# creative_group_id = '5982894'

# # test creative
# campaign_id = '5993656'
# alias = 'foobar'
# filename = 'rat-bus_259x194.jpg'
# creative_id = '5993692'
# creative_group_id = '5993689'

# agency_id = '5975435'
# get_agency_metrics_by_campaigns_current_day(agency_id)

# # #upload_creative(campaign_id, filename)

# campaign_id = '5982878'
# alias = 'testing'
# filename = 'rat-beetle_259x195.jpg'
# creative_id = '5982897'
# creative_group_id = '5982894'
# increment_creative_version(creative_id, campaign_id, filename, creative_group_id, alias=None)

# print(str(get_creative_version(creative_id)))
# --------------------------------------------------------------------------------------------------
