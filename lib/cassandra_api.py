# ------------------------------------------------------------------------------
#
# Connects to a Cassandra cluster, establishes a session, executes query, returns results
#
# Documentation:  http://datastax.github.io/python-driver/api/index.html
#
# ------------------------------------------------------------------------------
from cassandra.cluster import Cluster
from cassandra.cluster import Session

# default cassandra environment variables
#dns_name = 'us-east-1-dev-core-cassandra-audience-dev.trueffect.io'
dns_name = 'us-east-1-dev-core-cassandra-thames-dev.trueffect.io'
keyspace = 'ta_stg'

# static objects
cass_cluster = Cluster
cass_session = Session

# print('cass_cluster.is_shutdown: ' + str(cass_cluster.is_shutdown))
# print('cass_session.is_shutdown: ' + str(cass_session.is_shutdown))


# ------------------------------------------------------------------------------
# get/set cassandra environment variables
# ------------------------------------------------------------------------------


# set the cassandra cluster dns name
def set_cassandra_cluster_dns_name(dns_name_in):
    global dns_name
    dns_name = dns_name_in
    return get_cassandra_cluster_dns_name()
# ------------------------------------------------------------------------------
# get the cassandra cluster dns name
def get_cassandra_cluster_dns_name():
    global dns_name
    return dns_name

# ------------------------------------------------------------------------------
# set cassandra keyspace
def set_cassandra_keyspace(keyspace_in):
    global keyspace
    keyspace = keyspace_in
# ------------------------------------------------------------------------------
# get cassandra keyspace
def get_cassandra_keyspace():
    global keyspace
    return keyspace

# ------------------------------------------------------------------------------
def print_cassandra_env_vars():
    print('-------------------------------------------------------------------')
    print('dns_name: ' + dns_name)
    print('keyspace: ' + keyspace)


# ------------------------------------------------------------------------------
# cassandra helpers
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# connects to cassandra cluster, returns the cluster objected
def connect_to_cluster():
    method_def = 'connect_to_cluster()'
    print('==>  in: ' + method_def)
    dns_name = get_cassandra_cluster_dns_name()
    print(' - dns_name: ' + dns_name)

    global cass_cluster
    # global cass_session

    # # close the session if it is already running
    # if not cass_session.is_shutdown:
    #     print('Session is already running, shutting down...')
    #     cass_session.shutdown()
    #     print(' - done')
    #
    # # close the cluster if it is already running
    # if not cass_cluster.is_shutdown:
    #     print('Cluster is already running, shutting down...')
    #     cass_cluster.shutdown()
    #     print(' - done')

    # connect to cassandra cluster
    try:
        print('Connecting to Cluster...')
        cass_cluster = Cluster([dns_name])
        print(' - done!')
    except Exception:
        print('EXCEPTION: DNS Name Not Known: ' + dns_name)
        print('<== out: ' + method_def)
        exit(1)

    print('<== out: ' + method_def)
    return cass_cluster

# ------------------------------------------------------------------------------
# connects to cassandra cluster, establishes a session and returns the session
def connect_to_keyspace():
    method_def = 'connect_to_keyspace()'
    print('==>  in: ' + method_def)
    keyspace_name = get_cassandra_keyspace()
    print(' - keyspace_name: ' + keyspace_name)

    global cass_cluster
    global cass_session

    # validate cluster object
    if not cass_cluster:
        print('Error: cluster object is invalid!')
        print('<== out: ' + method_def)
        exit(1)

    # establish a new session with the cluster's keyspace
    try:
        print('Connecting to Session...')
        cass_session = cass_cluster.connect(keyspace_name)
        print(' - Done!')
    except Exception:
        print('EXCEPTION: Keyspace Not Known: ' + keyspace_name)
        print('<== out: ' + method_def)
        exit(2)

    print('<== out: ' + method_def)
    return cass_session


# ------------------------------------------------------------------------------
# cassandra queries
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# get I,C counts for a given campaign_id above the high water mark
#   returns a list containing the impression and click counts as ints
def get_cassandra_counts(campaign_id, highwater_mark_hr_id):
    global cass_cluster
    global cass_session

    cass_cluster = connect_to_cluster()
    cass_session = connect_to_keyspace()

    # query to execute
    qry = 'select * from hourly_campaign_counts where campaign_id = ' + str(campaign_id) + ' allow filtering'

    # impression & click counts
    i_count = 0
    c_count = 0

    # execute query
    try:
        # execute query
        print('Executing query: ' + qry)
        rows = cass_session.execute(qry)

        # get counts above the high water mark
        print('highwater_mark_hr_id: ' + str(highwater_mark_hr_id))
        for row in rows:
            # print('##############################')
            # print('row: ' + str(row))
            if row.hour_id > int(highwater_mark_hr_id):
                print(' > adding row to counts:')
                i_count += row.impressions
                c_count += row.clicks
                # print('   - adding impressions: ' + str(row.impressions))
                # print('   - adding clicks: ' + str(row.clicks))
                print('   - new i_count: ' + str(i_count))
                print('   - new c_count: ' + str(c_count))
    finally:
        # shutdown connections
        cass_session.shutdown()
        cass_cluster.shutdown()

    print('returning i_count: ' + str(i_count))
    print('returning c_count: ' + str(c_count))
    return [i_count, c_count]

# ------------------------------------------------------------------------------
# get I,C counts for a given campaign_id for the current UTC day
#   returns a list of lists [[hour_id, impressions, clicks], ...]
def get_cassandra_counts_intraday(campaign_id, utc_dy):
    print('############################################################')
    print('Getting cassandra counts for intraday Campaign')
    print(' - campaign_id: ' + str(campaign_id))
    print(' - utc_dy:      ' + str(utc_dy))

    # list of lists to return [[hour_id, impressions, clicks], ...]
    list_of_lists = []

    # query to execute
    qry = 'select * from hourly_campaign_counts where campaign_id = ' + str(campaign_id) + ' allow filtering'

    # get Cluster object
    cluster = None
    try:
        cluster = Cluster([dns_name])
    except Exception:
        print('DNS Name Not Known: ' + dns_name)
        exit(1)
    try:
        # get Session object
        session = None
        try:
            session = cluster.connect(keyspace)
        except Exception:
            print('Keyspace Not Known: ' + keyspace)
            exit(2)

        # execute query

        # get counts for the current UTC day
        print('Executing query: ' + qry)
        rows = session.execute(qry)
        for row in rows:
            if row.hour_id >= int(utc_dy + '00'):
                #print(row)
                list_of_lists.append([row.hour_id, row.impressions, row.clicks])
    finally:
        # shutdown connection
        cluster.shutdown()

    # return list of lists
    print('Returning list of lists:')
    print(list_of_lists)
    return list_of_lists

# ------------------------------------------------------------------------------
# get I,C counts for a given agency_id and list of campaigns for the current UTC day
#   returns a list of lists [[campaign_id, hour_id, impressions, clicks], ...]
def get_cassandra_counts_intraday_agency(campaign_list, utc_dy):
    print('############################################################')
    print('Getting cassandra counts for intraday Agency')
    print(' - campaign_list: ' + str(campaign_list))
    print(' - utc_dy:      ' + str(utc_dy))

    # list of lists to return [[hour_id, impressions, clicks], ...]
    list_of_lists = []

    # query to execute
    print(str(campaign_list))
    qry = 'select * from hourly_campaign_counts where day_id = ' + str(utc_dy) + ' and campaign_id in ' + str(campaign_list) + ' allow filtering'
    # get Cluster object
    cluster = None
    try:
        cluster = Cluster([dns_name])
    except Exception:
        print('DNS Name Not Known: ' + dns_name)
        exit(1)
    try:
        # get Session object
        session = None
        try:
            session = cluster.connect(keyspace)
        except Exception:
            print('Keyspace Not Known: ' + keyspace)
            exit(2)

        # execute query

        # get counts for the current UTC day
        print('Executing query: ' + qry)
        rows = session.execute(qry)
        for row in rows:
            if row.hour_id >= int(utc_dy + '00'):
                #print(row)
                list_of_lists.append([row.campaign_id, row.hour_id, row.impressions, row.clicks])
    finally:
        # shutdown connection
        cluster.shutdown()

    # return list of lists
    print('Returning list of lists:')
    print(list_of_lists)
    return list_of_lists

# ------------------------------------------------------------------------------
# get I,C counts for a given UTC day and list of placements
#   returns a list of lists [[campaign_id, hour_id, impressions, clicks], ...]
def get_cassandra_counts_intraday_placement(placement_list, utc_dy, utc_hr):
    print('==>  in: get_cassandra_counts_intraday_placement(placement_list, utc_dy, utc_hr)')
    print('############################################################')
    print('Getting cassandra counts for intraday Placement')
    print(' - placement_list: ' + str(placement_list))
    print(' - utc_dy:         ' + str(utc_dy))
    print(' - utc_hr:         ' + str(utc_hr))

    global cass_cluster
    global cass_session

    cass_cluster = connect_to_cluster()
    cass_session = connect_to_keyspace()

    # list for cassandra results
    result_list = []        # list of query results (rows)
    return_list = []        # list to return

    # query to execute
    qry = 'select * from hourly_placement_counts where day_id = ' + str(utc_dy) + ' and placement_id in (' + str(placement_list)[1:-1] + ')'

    # execute query
    try:
        print('Executing query: ' + qry)
        rows = cass_session.execute(qry)
        for row in rows:
            # print('++++++++++++++++++++++++++++')
            # print(row)
            placement_id = row.placement_id
            #day_id = row.day_id
            hour_id = row.hour_id
            clicks = row.clicks
            impressions = row.impressions
            result_list.append([placement_id, hour_id, impressions, clicks])
    finally:
        # shutdown connections
        cass_session.shutdown()
        cass_cluster.shutdown()

    # aggregate the counts by placement and add them to the list of lists
    # print('list_of_lists before aggregation:')
    # print('list length: ' + str(len(lol_unagg)))
    # print(lol_unagg)
    for placement in placement_list:
        print('Aggregating placement: ' + str(placement))
        currentImpressions = -1
        currentClicks = -1
        dailyImpressions = 0
        dailyClicks = 0
        for list in result_list:
            # print('list[0]: ' + str(list[0]))
            # print('placement: ' + str(placement))
            if str(list[0]) == str(placement):
                # print('*************************')
                # print(list)
                if currentImpressions < 0:
                    # print(' - setting current counts...')
                    currentImpressions = list[2]
                    currentClicks = list[3]
                else:
                    dailyImpressions += list[2]
                    dailyClicks += list[3]
        return_list.append([placement, currentImpressions, currentClicks, dailyImpressions, dailyClicks])

    # return list of lists
    lol = sorted(return_list)
    print('Returning list of lists:')
    print(lol)
    print('<== out: get_cassandra_counts_intraday_placement(placement_list, utc_dy, utc_hr)')
    return lol


# ------------------------------------------------------------------------------
def get_cassandra_timestamp():
    # query to execute
    qry = 'select * from meta_data'

    # timestamp
    tstamp = None

    # get Cluster object
    cluster = None
    try:
        cluster = Cluster([dns_name])
    except Exception:
        print('DNS Name Not Known: ' + dns_name)
        exit(1)
    try:
        # get Session object
        session = None
        try:
            session = cluster.connect(keyspace)
        except Exception:
            print('Keyspace Not Known: ' + keyspace)
            exit(2)

        # execute query
        #print('Executing query: ' + qry)
        result = session.execute(qry)
        tstamp = result[0].max_trans_time
        #print(tstamp)

    finally:
        # shutdown connection
        cluster.shutdown()

    return tstamp

# ------------------------------------------------------------------------------

# # ts = get_cassandra_timestamp()
# # print(ts)
#
# print_cassandra_env_vars()
#
# campaign_id = '5982878'
# highwater_mark_hr_id = '20160914'
#
# counts = get_cassandra_counts(campaign_id, highwater_mark_hr_id)
# i = counts[0]
# c = counts[1]
# print('i: ' + str(i))
# print('c: ' + str(c))

# ------------------------------------------------------------------------------



