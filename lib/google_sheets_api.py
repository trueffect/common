#!/usr/local/bin/python3
# ------------------------------------------------------------------------------
#
# Data Access for Google Sheets using gspread:
#   http://gspread.readthedocs.io/en/latest/
#
# oauth2 client docs:
#   http://oauth2client.readthedocs.io/en/latest/py-modindex.html
#
# Google API credentials management:
#   https://console.developers.google.com/permissions/serviceaccounts?project=te-automation
#
# IMPORTANT NOTE: In order to access a Google Sheet file from Google Drive,
#   the Google Sheet file must be shared with "te-automation@appspot.gserviceaccount.com"
#
# ------------------------------------------------------------------------------


import os
from oauth2client.service_account import ServiceAccountCredentials
from colorama import Fore, Back, Style, init
import gspread

import google_drive_api

# reset output color/style after each print
init(autoreset=True)

# google credentials to access google sheets from google drive
SCOPES = ['https://spreadsheets.google.com/feeds']
MY_PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
#JSON_CREDS = MY_PROJECT_DIR + os.sep + 'data' + os.sep + 'te-automation-google-creds.json'
JSON_CREDS = MY_PROJECT_DIR + os.sep + 'data' + os.sep + 'te-eng-svc-acct-sheets.json'


# -----------------------------------------------------------
# Returns a list of lists containing all cells’ values as strings.
def read_worksheet(spreadsheet_id, worksheet_name, log_info=True):
    return read_worksheet_range(spreadsheet_id, worksheet_name, 1, 1, 0, 0, log_info)


# -----------------------------------------------------------
# Returns a list of gspread.Cell objects from specified range.
#   http://gspread.readthedocs.io/en/latest/#gspread.Worksheet.range
#   http://gspread.readthedocs.io/en/latest/#gspread.Cell
def read_worksheet_range(spreadsheet_id, worksheet_name, first_row, first_col, last_row, last_col, log_info=True):

    # get Worksheet object
    ws_obj = get_gspread_worksheet_obj(spreadsheet_id, worksheet_name, log_info)
    if not ws_obj:
        return None

    # get list of lists (where each sublist is a list of row values)
    list_of_lists = ws_obj.get_all_values()
    last_used_row = len(list_of_lists)
    last_used_col = len(list_of_lists[1])
    if log_info:
        print(' > data row count:     ' + str(last_used_row))
        print(' > data col count:     ' + str(last_used_col))
        print('----------------------------------------')

    # calculate bottom right corner of range
    #   if 0, get used range (cells with values)
    if last_row == 0:
        last_row = last_used_row
    if last_col == 0:
        last_col = last_used_col

    list_of_list_vals = list_of_lists_range(list_of_lists, first_row, first_col, last_row, last_col)
    return list_of_list_vals


# -----------------------------------------------------------
# Returns a gspread.Worksheet object:
#   http://gspread.readthedocs.io/en/latest/#gspread.Worksheet
def get_gspread_worksheet_obj(spreadsheet_id, worksheet_name, log_info):

    # get Spreadsheet object
    spreadsheet_obj = gspread_spreadsheet(spreadsheet_id, log_info)
    if not spreadsheet_obj:
        return None

    # get Worksheet object
    worksheet_obj = gspread_worksheet(spreadsheet_obj, worksheet_name, log_info)
    if not worksheet_obj:
        return None

    # return Worksheet object
    return worksheet_obj



# -----------------------------------------------------------
# Returns a gspread.Spreadsheet object:
#   http://gspread.readthedocs.io/en/latest/#gspread.Spreadsheet
# Uses oauth2client to authenticate with google api:
#   http://oauth2client.readthedocs.io/en/latest/source/oauth2client.service_account.html#oauth2client.service_account.ServiceAccountCredentials.from_json_keyfile_name
# Google API service account managed here:
#   https://console.developers.google.com/permissions/serviceaccounts?project=te-automation
def gspread_spreadsheet(spreadsheet_id, log_info):
    # log info
    if log_info:
        print('========================================')
        print('Getting Spreadsheet Object...')
        print(' > spreadsheet_id: ' + spreadsheet_id)

    # get credentials
    google_api_creds = get_creds(log_info)

    # get Spreadsheet object
    try:
        ss_obj = google_api_creds.open_by_key(spreadsheet_id)
        if log_info:
            print(' > spreadsheet title:  ' + ss_obj.title)
    except gspread.SpreadsheetNotFound:
        print(Fore.WHITE + Back.RED + '[EXCEPTION] Spreadsheet not found, invalid ID: ' + Style.BRIGHT + str(spreadsheet_id))
        return None

    # return Spreadsheet object
    return ss_obj


# -----------------------------------------------------------
def get_creds(bool_log_info):
    #return google_drive_api.get_credentials()
    # log info
    # if bool_log_info:
    print('========================================')
    print('Getting credentials...')
    print(' > JSON_CREDS: ' + JSON_CREDS)
    print(' > SCOPES:     ' + str(SCOPES))

    # get service account credentials
    try:
        credentials = ServiceAccountCredentials.from_json_keyfile_name(JSON_CREDS, SCOPES)
    except gspread.AuthenticationError as ex:
        print(Fore.WHITE + Back.RED + '[Exception] Invalid Authorization: ' + Style.BRIGHT + str(JSON_CREDS))
        print(str(ex))
        return None
    return gspread.authorize(credentials)


#  -----------------------------------------------------------
# Returns a gspread.Worksheet object:
#   http://gspread.readthedocs.io/en/latest/#gspread.Worksheet
def gspread_worksheet(spreadsheet_obj, worksheet_name, log_info):

    # validate spreadsheet_obj exists
    if not spreadsheet_obj:
        print(Fore.RED + '[ERROR] Invalid Spreadsheet Object!')
        return None

    # get worksheet object
    try:
        ws_obj = spreadsheet_obj.worksheet(worksheet_name)    # gets by name
        if log_info: print(' > worksheet title:    ' + Style.BRIGHT + Fore.YELLOW + ws_obj.title + Style.RESET_ALL)
    except gspread.WorksheetNotFound:
        print(Fore.WHITE + Back.RED + '[EXCEPTION] Worksheet not found, invalid name: ' + Style.BRIGHT + str(worksheet_name))
        return None

    # return Worksheet object
    return ws_obj


# -----------------------------------------------------------
# Returns a list of lists containing all cells’ values as strings, within a range.
def list_of_lists_range(list_of_lists_in, first_row, first_col, last_row, last_col):

    # cut the range
    list_of_lists = []
    rows = list_of_lists_in[first_row-1:last_row]           # cut rows
    for row in rows:
        list_of_lists.append(row[first_col-1:last_col])     # cut cols

    # return list
    return list_of_lists


# -----------------------------------------------------------

# ------------------------------------------------------------------------------
