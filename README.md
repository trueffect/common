# google_sheets_api.py

This is a wrapper library that uses the following components:

1) gspread: `http://gspread.readthedocs.io/en/latest/`

2) oauth2client: `http://oauth2client.readthedocs.io/en/latest/py-modindex.html`

3) cassandra-driver: `pip3 install cassandra-driver` on mercury as prodman: `sudo /usr/local/python/bin/pip3 install cassandra-driver`

Access to Google Sheets on Google Drive is managed using a service account managed here:

https://console.developers.google.com/permissions/serviceaccounts?project=te-automation

Owners are tellebracht, gburke, wbutler

## IMPORTANT NOTE: In order to access a Google Sheet file from Google Drive, the Google Sheet file must be shared with "te-automation@appspot.gserviceaccount.com"
