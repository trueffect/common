#!/usr/bin/env bash
# ------------------------------------------------------------------------------
#
# Script compares two strings or two files and outputs the differences
# to the terminal with the changed groups colored for easy identification
#
# ------------------------------------------------------------------------------
# tput colors:
# 0 – Black
# 1 – Red
# 2 – Green
# 3 – Yellow
# 4 – Blue
# 5 – Magenta
# 6 – Cyan
# 7 – White
# ------------------------------------------------------------------------------

# Using stdin input, outputs each char. on its own line, with actual newlines
# in the input represented as literal '\n'.
toSingleCharLines() {
  sed 's/\(.\)/\1\'$'\n''/g; s/\n$/\'$'\n''\\n/'
}

# Using stdin input, reassembles a string split into 1-character-per-line output
# by toSingleCharLines().
fromSingleCharLines() {
  awk '$0=="\\n" { printf "\n"; next} { printf "%s", $0 }'
}

# Prints a colored string read from stdin by interpreting embedded color references such
# as '${RED}'.
printColored() {
  local str=$(</dev/stdin)
  local ORIG="$(tput setaf 1)" NEW="$(tput setaf 4)" RST="$(tput sgr0)" BG="$(tput setab 7)"
  str=${str//'${BG}'/${BG}}
  str=${str//'${ORIG}'/${ORIG}}
  str=${str//'${NEW}'/${NEW}}
  str=${str//'${RST}'/${RST}}
  printf '%s\n' "$str"
}

# check for command line args
if [[ $# != 3 ]]; then
	me=`basename "$0"`
	echo " - Usage: $me -f actual_result_file expected_result_file"
	echo " - Usage: $me -s actual_result_string expected_result_string"
	exit
fi

# check flags
if [[ $1 == -f ]]; then
	#echo "file flag"
	# The non-ASCII input string.
	diff_val_1=`cat $2`
	# Create its ASCII-chars.-only transliteration.
	diff_val_2=`cat $3`
	# description for output
	val_1_desc=$2
	val_2_desc=$3
elif [[ $1 == -s ]]; then
	#echo "string flag"
	# The non-ASCII input string.
	diff_val_1=$2
	# Create its ASCII-chars.-only transliteration.
	diff_val_2=$3
	# description for output
	val_1_desc="Actual Result"
	val_2_desc="Expected Result"
else
	echo "Error: Invalid flag, must be -f (for files) or -s (for strings)"
	me=`basename "$0"`
	echo " - Usage: $me -f actual_result_file expected_result_file"
	echo " - Usage: $me -s actual_result_string expected_result_string"
	exit
fi

# Print the ORIGINAL string with the characters that NEED transliteration
# highlighted in ORIG color.
echo "=== $val_1_desc ==="
diff --changed-group-format='${BG}${ORIG}%=${RST}' \
  <(toSingleCharLines <<<"$diff_val_1") <(toSingleCharLines <<<"$diff_val_2") |
    fromSingleCharLines | printColored

# Print the TRANSLITERATED string with the characters that RESULT FROM
# transliteration highlighted in NEW color.
# echo ""
echo "=== $val_2_desc ==="
diff --changed-group-format='${BG}${NEW}%=${RST}' \
  <(toSingleCharLines <<<"$diff_val_2") <(toSingleCharLines <<<"$diff_val_1") |
    fromSingleCharLines | printColored

# ------------------------------------------------------------------------------
